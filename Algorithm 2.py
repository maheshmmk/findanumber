from math import sqrt
revnum = 0
num = 999999999
while(num>=123456780):
    result = sqrt(num)
    # Check for a whole number
    if (result%1 == 0):
        # Compare digits
        digits = set(int(n) for n in str(num))
        if (len(digits) == len(str(num))):
            print("Number with distinct digits and square number: ", num)
            # Reverse the number 
            while(num%10 != 0):
                c = num%10
                revnum = revnum*10 + c
                num=num//10
            print("Largest number from left to right:", revnum)
            break
    num = num-1


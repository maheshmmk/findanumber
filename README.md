# Find a number

The number satisfies 3 conditions
1. 9 digit number with distinct digits.
2. From right to left it is a whole square number.
3. From left to right it is the highest number and satisfies both the above 2 criteria.